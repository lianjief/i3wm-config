#!/usr/bin/zsh python3
#
# Copyright (C) 2017 Lianjie Fan
#
# A battery indicator script for i3blocks

from subprocess import check_output


brightness = check_output(['xbacklight'], universal_newlines=True).replace("\n", "")

if brightness:
    print(int(float(brightness)), "%")


